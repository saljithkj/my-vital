package org.sample.my.vital.live.Room;

import androidx.annotation.Keep;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Keep
@Entity
public class BPmodel {
    @PrimaryKey(autoGenerate = true)
    private long id;

    private String bpvalue;




    public String getBpvalue() {
        return bpvalue;
    }

    public void setBpvalue(String bpvalue) {
        this.bpvalue = bpvalue;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
