package org.sample.my.vital.live.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.sample.my.vital.live.R;
import org.sample.my.vital.live.Room.BPmodel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddmyVital#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddmyVital extends Fragment implements Contract.View {


    Unbinder unbinder;

    @BindView(R.id.edit_vital)
    EditText edit_vital;
    @BindView(R.id.btn_addvital)
    Button addvital;

    MyVitalPresenter myVitalPresenter;



    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AddmyVital() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddmyVital.
     */
    // TODO: Rename and change types and number of parameters
    public static AddmyVital newInstance(String param1, String param2) {
        AddmyVital fragment = new AddmyVital();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myVitalPresenter=new MyVitalPresenter();
        myVitalPresenter.setView(this);
        myVitalPresenter.setModel(new MyvitalModel(getActivity().getApplication()));
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_addmy_vital,
                container, false);
        unbinder = ButterKnife.bind(this, rootView);
        // Inflate the layout for this fragment

       addvital.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if (Integer.parseInt(edit_vital.getText().toString())<90||Integer.parseInt(edit_vital.getText().toString())>250)
               {
                   Toast.makeText(getContext(), "value is out of the range", Toast.LENGTH_SHORT).show();
               }else {
                  BPmodel bPmodel=new BPmodel();
                  bPmodel.setBpvalue(edit_vital.getText().toString());
                  myVitalPresenter.storeData(bPmodel);
               }


           }
       });



        return rootView;
    }

    @Override
    public void getdetails(List<BPmodel> body) {

    }

    @Override
    public void showresults() {
        Toast.makeText(getContext(), "Successfully Added", Toast.LENGTH_SHORT).show();
    }
}