package org.sample.my.vital.live.Fragments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.sample.my.vital.live.R;
import org.sample.my.vital.live.Room.BPmodel;

import java.util.ArrayList;
import java.util.List;

class TrendsAdapter extends RecyclerView.Adapter<TrendsAdapter.ViewHolder> {
    List<BPmodel> news = new ArrayList<>();
    Context context;
    public TrendsAdapter(List<BPmodel> news,Context context) {
        this.context=context;

        this.news=news;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_bp, parent, false);


        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TrendsAdapter.ViewHolder holder, int position) {
        holder.value.setText(news.get(position).getBpvalue());

    }




    @Override
    public int getItemCount() {
        return news.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView value;


        public ViewHolder(final View itemView) {
            super(itemView);
            value = itemView.findViewById(R.id.value);


        }
    }
}
