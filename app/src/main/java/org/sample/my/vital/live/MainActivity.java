package org.sample.my.vital.live;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;

import org.sample.my.vital.live.Fragments.AddmyVital;
import org.sample.my.vital.live.Fragments.HighBp;
import org.sample.my.vital.live.Fragments.Trends;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.main_container)
    FrameLayout mainContainer;
    @BindView(R.id.bottomNavigation)
    TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        tabLayout.addTab(tabLayout.newTab().setCustomView(getTabView(0)));
        tabLayout.addTab(tabLayout.newTab().setCustomView(getTabView(1)));
        tabLayout.addTab(tabLayout.newTab().setCustomView(getTabView(2)));



        View root = tabLayout.getChildAt(0);
        if (root instanceof LinearLayout) {
            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(ContextCompat.getColor(getApplicationContext(),R.color.colorAccent));
            drawable.setSize(0, 1);
            ((LinearLayout) root).setDividerPadding(10);
            ((LinearLayout) root).setDividerDrawable(drawable);
        }
        replaceFragment(new AddmyVital(), "AddMyVital", 0, true);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                    if (tab.getPosition() == 0) {
                        replaceFragment(new AddmyVital(), "AddMyVital", 0, true);
                    } else if (tab.getPosition() == 1) {
                        replaceFragment(new Trends(), "trends", 1, true);
                    } else if (tab.getPosition() == 2) {
                        replaceFragment(new HighBp(), "STAFF hifhbp", 2, true);

                    }




            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {


            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {


            }
        });



    }

    private void replaceFragment(Fragment fragment, String TAG, int selectedPosition, boolean addtoBackStack) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.main_container, fragment);
        if (addtoBackStack)
            transaction.addToBackStack(TAG);
        transaction.commit();
    }

    public View getTabView(int position) {
        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView tv = v.findViewById(R.id.txv_name);
        ImageView img = v.findViewById(R.id.imv_icon);


        switch (position) {
            case 0:

                tv.setText("Add Vital");

                break;

            case 1:

                tv.setText("Trend");

                break;

            case 2:

                tv.setText("High BP");

                break;



        }


        return v;
    }

}