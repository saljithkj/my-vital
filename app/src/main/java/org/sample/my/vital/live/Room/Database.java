package org.sample.my.vital.live.Room;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

@androidx.room.Database(
        entities = {BPmodel.class},
        version = 33,exportSchema = false
)
public abstract class Database extends RoomDatabase {

    private static Database INSTANCE;

    public static Database getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), Database.class, "Myvital_db")
                            .allowMainThreadQueries().fallbackToDestructiveMigration()
//                            .addMigrations(MIGRATION_1_2)
                            .build();
        }
        return INSTANCE;
    }

    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE IF NOT EXISTS `LubeType` " +
                    "(`id` INTEGER PRIMARY KEY NOT NULL, `name` TEXT, " +
                    "`image` TEXT,`stationid` TEXT,`amount` REAL NOT NULL)");
            database.execSQL("CREATE TABLE IF NOT EXISTS `LubeSales` " +
                    "(`appentryid` INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL, `id` INTEGER NOT NULL, " +
                    "`stationid` TEXT,`date` TEXT,`stafftype` TEXT ,`type` TEXT,`amount` TEXT,`username` TEXT,`notes` TEXT,`dutyid` TEXT,`noofslips` TEXT,`lubid` TEXT)");
        }
    };

    public abstract BPDao getBPDao();

}