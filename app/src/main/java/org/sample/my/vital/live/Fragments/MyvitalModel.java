package org.sample.my.vital.live.Fragments;

import android.app.Application;

import androidx.annotation.NonNull;

import org.sample.my.vital.live.Room.BPmodel;
import org.sample.my.vital.live.Room.DBCallback;
import org.sample.my.vital.live.Room.Database;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

class MyvitalModel implements Contract.Model {


    private Database appDatabase;

    public MyvitalModel(@NonNull Application application) {
        appDatabase = Database.getDatabase(application);
    }


    @Override
    public void getData(DBCallback dbCallback) {



        List<BPmodel> bPmodels=new ArrayList<>();
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {


                bPmodels.addAll(appDatabase.getBPDao().GetBP());



            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onComplete() {
                if (dbCallback != null) {
                    dbCallback.onSuccess(bPmodels);
                }
            }

            @Override
            public void onError(Throwable e) {
                if (dbCallback != null) {
                    dbCallback.onFailure();
                }
            }
        });


    }

    @Override
    public void storeData(final BPmodel bPmodel, final DBCallback dbCallback) {


        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                long id =  appDatabase.getBPDao().InsertBP(bPmodel);

            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onComplete() {
                if (dbCallback != null) {
                    dbCallback.onSuccess("Success");
                }
            }

            @Override
            public void onError(Throwable e) {
                if (dbCallback != null) {
                    dbCallback.onFailure();
                }
            }
        });





    }
}
