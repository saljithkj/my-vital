package org.sample.my.vital.live.Room;

public interface DBCallback<T> {
    void onSuccess(T data);
    void onFailure();

}
