package org.sample.my.vital.live.Room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface BPDao {
//
    @Insert(onConflict = REPLACE)
    long InsertBP(BPmodel bPmodel);

    @Query("select * from BPmodel ORDER BY id")
    List<BPmodel> GetBP();

    @Query("Delete from BPmodel")
    void deleteAll();


}
