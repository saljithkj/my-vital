package org.sample.my.vital.live.Fragments;

import org.sample.my.vital.live.Room.BPmodel;
import org.sample.my.vital.live.Room.DBCallback;

import java.util.List;

class MyVitalPresenter implements Contract.Presenter {
    Contract.View view;
    Contract.Model model;

    @Override
    public void setModel(Contract.Model model) {
    this.model=model;
    }

    @Override
    public void setView(Contract.View view) {
        this.view=view;
    }

    @Override
    public void getdata() {
        model.getData(new DBCallback<List<BPmodel>>() {
            @Override
            public void onSuccess(List<BPmodel> bPmodel) {

                view.getdetails(bPmodel);

            }

            @Override
            public void onFailure() {

            }
        });

    }

    @Override
    public void storeData(BPmodel bp) {


        this.model.storeData(bp, new DBCallback<String>() {
            @Override
            public void onSuccess(String staus) {

                view.showresults();
            }

            @Override
            public void onFailure() {

                //  view.dismissProgress();
            }
        });
    }
    }

