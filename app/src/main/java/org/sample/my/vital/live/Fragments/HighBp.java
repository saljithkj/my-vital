package org.sample.my.vital.live.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.sample.my.vital.live.R;
import org.sample.my.vital.live.Room.BPmodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HighBp#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HighBp extends Fragment implements Contract.View {
    Unbinder unbinder;
    @BindView(R.id.recycler_trends)
    RecyclerView recyclerView;

    MyVitalPresenter myVitalPresenter;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Trends.
     */
    // TODO: Rename and change types and number of parameters
    public static Trends newInstance(String param1, String param2) {
        Trends fragment = new Trends();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myVitalPresenter=new MyVitalPresenter();
        myVitalPresenter.setView(this);
        myVitalPresenter.setModel(new MyvitalModel(getActivity().getApplication()));
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_trends,
                container, false);
        unbinder = ButterKnife.bind(this, rootView);
        myVitalPresenter.getdata();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void getdetails(List<BPmodel> body) {


        List<BPmodel> bPmodels=new ArrayList<>();

        bPmodels=body;
        Collections.sort(bPmodels, new Comparator<BPmodel>() {
            @Override
            public int compare(BPmodel lhs, BPmodel rhs) {
                return lhs.getBpvalue().compareTo(rhs.getBpvalue());
            }
        });


        TrendsAdapter trendsAdapter = new TrendsAdapter(bPmodels, getContext());
        recyclerView.setAdapter(trendsAdapter);





    }

    @Override
    public void showresults() {

    }
}