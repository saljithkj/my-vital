package org.sample.my.vital.live.Fragments;

import org.sample.my.vital.live.Room.BPmodel;
import org.sample.my.vital.live.Room.DBCallback;

import java.util.List;

interface Contract {


    interface Model{
        void getData(final DBCallback dbCallback);
        void storeData(BPmodel bPmodel, final DBCallback dbCallback);

    }

    interface View {
        void getdetails(List<BPmodel> body);
        void showresults();
    }

    interface Presenter {
        void setModel(Model model);
        void setView(View view);
        void getdata();
        void storeData(BPmodel bPmodel);
    }


}
